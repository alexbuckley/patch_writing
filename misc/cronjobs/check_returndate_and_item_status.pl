#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME
check_returndate_and_item_status.pl - cron script to check if returndate is current date and if item damaged status is 'Claims return' if both conditions are true then the item damaged status is set to 0 (default value).

=head1 SYNOPSIS

./check_returndate_and_item_status.pl -c

or, in /etc/cron.d/koha-common (to run this script automatically at 6pm every day):
* 18 * * *  root koha-foreach --enabled /usr/share/koha/bin/cronjobs/check_returndate_and_item_status.pl


=head1 DESCRIPTION

This cronjob script is to be run at the end of each working day. It is to check the returndate of all items in the issues table (i.e. all items currently checked out) to check if their returndate is the current date (i.e. the item has been returned on the current date) and if the items damaged status is 'Claims return'. If both these conditions are true then the item damaged status is set to 0

=cut

use strict;
use warnings;
use Getopt::Long;
use Data::Dumper;
use POSIX qw(strftime);
use DateTime;
BEGIN {
    # find Koha's Perl modules
    # test carefully before changing this
    use FindBin;
    eval { require "$FindBin::Bin/../kohalib.pl" };
}
use C4::Context;

=head1 NAME

check_returndate_and_item_status.pl - Change item status of items if they have been returned today and have the item status of 'Claims return'

=head1 SYNOPSIS

check_returndate_and_item_status.pl 

=head1 DESCRIPTION

This script is designed to change the damaged status of an item if it's current damaged status is 'Claims return' and it was returned on the current date

=cut

binmode( STDOUT, ':encoding(UTF-8)' );

my $dbh = C4::Context->dbh();

my $sth = $dbh->prepare(<<'END_SQL');
    SELECT authorised_value FROM authorised_values WHERE category="DAMAGED" && lib="Claims return"
END_SQL
$sth->execute();
my $auth_value = $sth->fetchrow;

my $itemsth = $dbh->prepare(<<'END_SQL');
    SELECT statistics.itemnumber, statistics.datetime, items.damaged FROM statistics LEFT JOIN items ON statistics.itemnumber = items.itemnumber WHERE statistics.type="return"
END_SQL

$itemsth->execute();
while ( my $issue = $itemsth->fetchrow_hashref()) {
    my $itemnumber= $issue->{itemnumber};
    my $returndate = $issue->{datetime};
    my ($date, $time) = split (/ /, $returndate);

    my $damage_status = $issue->{damaged};
    my $todays_date = strftime "%Y-%m-%d", localtime;

    if ($damage_status == $auth_value && $date eq $todays_date ) {
$sth = $dbh->prepare(<<'END_SQL');
            UPDATE items SET damaged = 0 WHERE itemnumber = ?
END_SQL
        $sth->execute($itemnumber);
        warn "Changed damaged status for itemnumber $itemnumber";
    }
}

1;

__END__
